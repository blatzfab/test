#include <QtCore>
#include <QtBluetooth>
#include <QtXml>
#include <QtNetwork>
#include <QtSerialPort>


class Task : public QObject
{
	Q_OBJECT
public:
	Task(QObject *parent = 0) : QObject(parent), m_parent(parent) {}

public slots:
	void run()
	{
		// Do processing here: just try to instatiate various base objects, to see if linking works as expected:

		QLowEnergyController *leCtrl = QLowEnergyController::createPeripheral(m_parent);

		QXmlSimpleReader *xmlReader = new QXmlSimpleReader();
		QXmlStreamReader *xmlStreamReader = new QXmlStreamReader();

		QSerialPort *sPort = new QSerialPort(m_parent);

		QNetworkAccessManager *netMgr = new QNetworkAccessManager(m_parent);
		if (netMgr->networkAccessible() == QNetworkAccessManager::Accessible)
		{
			qDebug() << "Network is accessible";
		}
		else
		{
			qDebug() << "Network is not accessible";
		}

		qDebug() << "closing down";
		emit finished();
	}

signals:
	void finished();

private:
	QObject *m_parent;
};

#include "main.moc"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	qDebug() << "argc: " << argc;
	int i = 0;
	for (i = 0; i < argc; ++i)
	{
		qDebug() << "arg " << i << ": " << argv[i];
	}


	// Task parented to the application so that it
	// will be deleted by the application.
	Task *task = new Task(&a);

	// This will cause the application to exit when
	// the task signals finished.
	QObject::connect(task, SIGNAL(finished()), &a, SLOT(quit()));

	// This will run the task from the application event loop.
	QTimer::singleShot(0, task, SLOT(run()));

	qDebug() << "Started test task ..";

	return a.exec();
}
